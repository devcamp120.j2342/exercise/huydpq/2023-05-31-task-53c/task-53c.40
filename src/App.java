import models.Circle;
import models.Retangle;

public class App {
    public static void main(String[] args) throws Exception {
        Retangle retangle = new Retangle(2, 3);
        Circle circle = new Circle(2);

        System.out.println(retangle);
        System.out.println(circle);

        System.out.println("Hình chữ nhật");
        System.out.println(retangle.getArea());
        System.out.println(retangle.getPerimeter());

        System.out.println("Hình tròn");
        System.out.println(retangle.getArea());
        System.out.println(retangle.getPerimeter());

    }
}
