package models;
import interfaces.GeometricObject;

public class Circle implements GeometricObject {
    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    // diện tích hình tròn
    public double getArea(){
       return Math.PI * Math.pow(this.radius,2);
    }
    // chu vi hình tròn
    public double getPerimeter(){
       return Math.PI * this.radius *2 ;
    }

    @Override
    public String toString() {
        return "Circle [radius=" + radius + "]";
    }

    
}
