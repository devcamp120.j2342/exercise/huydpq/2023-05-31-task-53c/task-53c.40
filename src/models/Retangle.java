package models;

import interfaces.GeometricObject;
public class Retangle implements GeometricObject {
    private double width;
    private double heigth;


    public Retangle(double width, double heigth) {
        this.width = width;
        this.heigth = heigth;
    }

    // diện tích hình chữ nhật
    public double getArea(){
        return this.heigth * this.width;
    }

    //  chu vi  hình chữ nhật
    public double getPerimeter(){
        return (this.heigth + this.width) * 2;
    }

    @Override
    public String toString() {
        return "Retangle [width=" + width + ", heigth=" + heigth + "]";
    }

    
}
